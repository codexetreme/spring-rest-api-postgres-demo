
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";


CREATE SEQUENCE user_id START 1;

CREATE TABLE user_data (

    id INTEGER default nextval('user_id') PRIMARY KEY ,
    uuid UUID default uuid_generate_v4(),
    name text not null,
    email text not null,
    dob date not null,
    status boolean default false
);


CREATE TABLE friendship(
    owner_id integer NOT NULL,
    friend_id integer NOT NULL,
    request_accepted boolean default FALSE,
    PRIMARY KEY (owner_id, friend_id),
    FOREIGN KEY (owner_id) REFERENCES user_data (id),
    FOREIGN KEY (friend_id) REFERENCES user_data (id)



);




CREATE SEQUENCE leaderboard_id START 1;

CREATE TABLE leaderboard (

   id INTEGER default nextval('leaderboard_id') PRIMARY KEY ,
   uuid UUID default uuid_generate_v4(),
   user_id integer not null ,
   rank integer not null,
   calories_burnt double precision not null,
   heart_rate double precision not null,
   workout_duration double precision not null,
   FOREIGN KEY (user_id) REFERENCES user_data (id)
);

