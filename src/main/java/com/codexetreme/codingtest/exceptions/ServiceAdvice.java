package com.codexetreme.codingtest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice

public class ServiceAdvice {

    @ResponseBody
    @ExceptionHandler(ServiceImproperResponseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String errorHandler(ServiceImproperResponseException e){
        System.out.println(e.getMessage());
        e.printStackTrace();
        return "There was an error please try again";
    }

}
