package com.codexetreme.codingtest.exceptions;

public class ServiceImproperResponseException extends RuntimeException {
    public ServiceImproperResponseException(String message) {
        super(message);
    }
}
