package com.codexetreme.codingtest.exceptions;

public class DataMalformedException extends RuntimeException{

    public DataMalformedException(String message) {
        super(message);
    }
}
