package com.codexetreme.codingtest.api;

import com.codexetreme.codingtest.exceptions.ServiceImproperResponseException;
import com.codexetreme.codingtest.models.User;
import com.codexetreme.codingtest.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/new")
    public void addNewUser(@RequestBody User newUser){
        try {
            userService.insert(newUser);
        } catch (ServiceImproperResponseException e){
            throw e;
        }
    }

    public void setUserOnlineStatus(){

    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") UUID id){
        try {
            userService.deleteRow(id);
        } catch (ServiceImproperResponseException e){
            throw e;
        }
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable("id") UUID id){
        try {
            return userService.getOne(id);
        } catch (ServiceImproperResponseException e){
            throw e;
        }
    }

    @GetMapping("/get-all")
    public List<User> getUsers(){
        try {
            return userService.getAll();
        } catch (ServiceImproperResponseException e){
            throw e;
        }
    }
}
