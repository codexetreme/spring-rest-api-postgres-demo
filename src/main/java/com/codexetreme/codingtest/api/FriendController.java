package com.codexetreme.codingtest.api;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/friend")
public class FriendController {

    @GetMapping
    public void getFriends(@PathVariable("id") UUID user_id){

    }

    public void sendFriendRequest(){

    }

    public void acceptFriendRequest(){

    }

    public void removeFriend(){

    }

}
