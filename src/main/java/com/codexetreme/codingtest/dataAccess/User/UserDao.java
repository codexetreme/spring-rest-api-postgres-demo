package com.codexetreme.codingtest.dataAccess.User;

import com.codexetreme.codingtest.dataAccess.ISqlCrudOperations;
import com.codexetreme.codingtest.exceptions.DataMalformedException;
import com.codexetreme.codingtest.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public class UserDao implements ISqlCrudOperations<User> {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insert(User data) throws DataMalformedException {
        String stmt = "INSERT INTO user_data(name, email, dob, status) VALUES (?,?,?,?)";
        try {
            jdbcTemplate.update(stmt, data.getName(), data.getEmail(), data.getDOB() ,false);
        } catch (DataAccessException e){
            throw new DataMalformedException(e.getMessage());
        }
    }

    @Override
    public User get(UUID id) throws DataMalformedException {
        String stmt = String.format("SELECT * from user_data where uuid = '%s'", id.toString());
        try {
            List<User> users = jdbcTemplate.query(stmt, (resultSet, i) -> {
                String name = resultSet.getString("name");
                String email = resultSet.getString("email");
                java.sql.Date dob = resultSet.getDate("dob");
                Boolean status = resultSet.getBoolean("status");
                return new User(id, email, name, new java.util.Date(dob.getTime()).toInstant().getEpochSecond(), status);
            });
            return users.get(0);
        } catch (DataAccessException e){
            throw new DataMalformedException(e.getMessage());
        }
    }

    @Override
    public List<User> getAll(int limit, int offset) throws DataMalformedException {
        String stmt = String.format("SELECT * from user_data LIMIT %s OFFSET %s", limit, offset);
        try {
            return jdbcTemplate.query(stmt, (resultSet, i) -> {
                String name = resultSet.getString("name");
                UUID id = UUID.fromString(resultSet.getString("uuid"));
                String email = resultSet.getString("email");
                java.sql.Date dob = resultSet.getDate("dob");
                Boolean status = resultSet.getBoolean("status");
                return new User(id, email, name, new java.util.Date(dob.getTime()).toInstant().getEpochSecond(), status);
            });
        } catch ( Exception e){
            throw new DataMalformedException(e.getMessage());
        }
    }

    @Override
    public void update(UUID id, User data) throws DataMalformedException{
        try {
            String stmt = new StringBuilder().append("UPDATE user_data SET ")
                    .append("name = '").append(data.getName()).append("'")
                    .append("email = '").append(data.getEmail()).append("'")
                    .append("dob = '").append(data.getDOB()).append("'")
                    .append("status = '").append(data.getStatus()).append("'").toString();

            jdbcTemplate.update(stmt);
        } catch (DataAccessException e){
            throw new DataMalformedException(e.getMessage());
        }
    }

    @Override
    public void delete(UUID id) throws DataMalformedException {
        String stmt = String.format("DELETE from user_data WHERE uuid = '%s'", id.toString());
        try {
            jdbcTemplate.update(stmt);
        } catch (DataAccessException e){
            throw new DataMalformedException(e.getMessage());
        }
    }
}
