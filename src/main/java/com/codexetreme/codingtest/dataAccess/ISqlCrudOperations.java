package com.codexetreme.codingtest.dataAccess;

import com.codexetreme.codingtest.exceptions.DataMalformedException;

import java.util.List;
import java.util.UUID;

public interface ISqlCrudOperations<T> {

    void insert(T data) throws DataMalformedException;
    T get(UUID id) throws DataMalformedException;
    List<T> getAll(int limit, int offset) throws DataMalformedException;
    default List<T> getAll() throws DataMalformedException {
        try {
            return getAll(100, 0);
        } catch (DataMalformedException e){
            System.out.println(e.getMessage());
            return null;
        }

    }
    void update(UUID id, T data) throws DataMalformedException;
    void delete(UUID id) throws DataMalformedException;

}
