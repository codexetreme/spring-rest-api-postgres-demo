package com.codexetreme.codingtest.services;

import com.codexetreme.codingtest.exceptions.ServiceImproperResponseException;

import java.util.List;
import java.util.UUID;

public interface IBaseService<T> {
    List<T> getAll(int limit, int offset) throws ServiceImproperResponseException;
    default List<T> getAll() throws ServiceImproperResponseException{
        try {
            return getAll(100, 0);
        } catch (ServiceImproperResponseException e){
            throw e;
        }
    }
    T getOne(UUID id) throws ServiceImproperResponseException;
    void updateRow(UUID id, T data) throws ServiceImproperResponseException;
    void deleteRow(UUID id) throws ServiceImproperResponseException;
    void insert(T data) throws ServiceImproperResponseException;

}
