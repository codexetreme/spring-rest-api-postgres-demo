package com.codexetreme.codingtest.services;

import com.codexetreme.codingtest.dataAccess.User.UserDao;
import com.codexetreme.codingtest.exceptions.DataMalformedException;
import com.codexetreme.codingtest.exceptions.ServiceImproperResponseException;
import com.codexetreme.codingtest.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserService implements IBaseService<User> {

    private final UserDao userDao;

    @Autowired
    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public List<User> getAll(int limit, int offset) throws ServiceImproperResponseException {
        try {
            return userDao.getAll(limit, offset);
        } catch (DataMalformedException e){
            throw new ServiceImproperResponseException(e.getMessage());
        }
    }

    @Override
    public User getOne(UUID id) throws ServiceImproperResponseException {
        try {
            return userDao.get(id);
        }
        catch (DataMalformedException e){
            throw new ServiceImproperResponseException(e.getMessage());
        }
    }

    @Override
    public void updateRow(UUID id, User data) throws ServiceImproperResponseException {
        try {
            userDao.update(id, data);
        } catch (DataMalformedException e){
            throw new ServiceImproperResponseException(e.getMessage());
        }
    }

    @Override
    public void deleteRow(UUID id) throws ServiceImproperResponseException {
        try {
            userDao.delete(id);
        } catch (DataMalformedException e){
            throw new ServiceImproperResponseException(e.getMessage());
        }
    }

    @Override
    public void insert(User data) throws ServiceImproperResponseException {
        try {
            userDao.insert(data);
        } catch (DataMalformedException e){
            throw new ServiceImproperResponseException(e.getMessage());
        }
    }
}
