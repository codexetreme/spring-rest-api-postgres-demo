package com.codexetreme.codingtest.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

public class User {

    private final UUID id;
    private final String Name;
    private final String Email;
    private final Date DOB;
    private final Boolean Status;

    public User(UUID id, String name, String email, @JsonProperty("dob") Long DOB, Boolean status) {
        this.id = id;
        Name = name;
        Email = email;
        this.DOB = Date.from(Instant.ofEpochSecond(DOB));
        this.Status = status;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return Name;
    }

    public String getEmail() {
        return Email;
    }

    public Date getDOB() {
        return DOB;
    }

    public Boolean getStatus() {
        return Status;
    }
}
