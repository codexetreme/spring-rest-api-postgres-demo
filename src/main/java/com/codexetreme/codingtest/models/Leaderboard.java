package com.codexetreme.codingtest.models;

import java.util.UUID;

public class Leaderboard {

    private UUID id;
    private User user;
    private Integer rank;
    private Double caloriesBurnt;
    private Double heartRate;
    private Double workoutDuration;


    public Leaderboard(UUID id, User user, Integer rank, Double caloriesBurnt, Double heartRate, Double workoutDuration) {
        this.id = id;
        this.user = user;
        this.rank = rank;
        this.caloriesBurnt = caloriesBurnt;
        this.heartRate = heartRate;
        this.workoutDuration = workoutDuration;
    }


    public UUID getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Integer getRank() {
        return rank;
    }

    public Double getCaloriesBurnt() {
        return caloriesBurnt;
    }

    public Double getHeartRate() {
        return heartRate;
    }

    public Double getWorkoutDuration() {
        return workoutDuration;
    }
}
