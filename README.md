# Java Spring Boot REST Api demo

simple example to show how to use postgres, jdbc and rest in spring-boot


## To run, in development mode

run these from the root directory

`./sh/compose_up.sh` -- starts the postgres docker

then simply import the project into intelliJ IDEA and run the auto generated build configuration
